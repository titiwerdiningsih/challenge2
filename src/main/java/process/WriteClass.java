package process;

import java.io.File;
import java.io.IOException;
import java.io.*;

public class WriteClass extends ProcessClass {
    private static String text = "D:\\IST FILE\\challenge02-main\\src\\main\\temp\\data.txt";
    private static String group = "D:\\IST FILE\\challenge02-main\\src\\main\\temp\\group.txt";
    private static String csvFile = "D:\\IST FILE\\challenge02-main\\src\\main\\temp\\data_sekolah.csv";

    public static void writeGroup(int input) {
        try {
            File file = new File(group);
            if(!file.exists()) {
                file.createNewFile();
            }
            ProcessInterface processInterface = new ProcessClass();
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("nilai yang lebih besar dari "+input);
            bw.newLine();
            bw.write(Integer.toString(Math.toIntExact(processInterface.groupHigher(csvFile, input))));
            bw.newLine();
            bw.write("nilai yang lebih kecil dari "+input);
            bw.newLine();
            bw.write(Integer.toString(Math.toIntExact(processInterface.groupLower(csvFile, input))));
            bw.flush();
            bw.close();
            System.out.println("sukses ditulis");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeMeanMedianModus() {
        try {
            File file = new File(text);
            if(!file.exists()) {
                file.createNewFile();
            }
            ProcessInterface processInterface = new ProcessClass();
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("nilai mean dari data siswa:");
            bw.newLine();
            bw.write(Double.toString(processInterface.generateMean(csvFile)));
            bw.newLine();
            bw.write("nilai midean dari data siswa:");
            bw.newLine();
            bw.write(Integer.toString(processInterface.generateMedian(csvFile)));
            bw.newLine();
            bw.write("nilai modus nilai siswa: ");
            bw.newLine();
            bw.write(Integer.toString(processInterface.generateModus(csvFile)));
            bw.flush();
            bw.close();
            System.out.println("sukses ditulis");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

