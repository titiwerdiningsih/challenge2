package menu;

import process.ProcessClass;
import process.WriteClass;
import java.util.Scanner;

public class MainMenu {

    private static String top = "----------------------------------------------------- ";
    private static String title = "Aplikasi Pengolahan Nilai Siswa";

    public static void cek(){

        System.out.println(top);
        System.out.println(title);
        System.out.println(top);
        System.out.println("Letakkan file csv dengan nama file data_sekolah di direktori");
        System.out.println("berikut: C://temp/direktori/\n");
        System.out.println("Tekan enter untuk melanjutkan");
        try {
            System.in.read();
        }
        catch (Exception e){
            System.out.println(e.getMessage());

        }
        mainMenu();

    }

    public static void mainMenu() {

        System.out.println(top);
        System.out.println(title);
        System.out.println(top);
        System.out.println("\nPilih menu: ");
        System.out.println("1. Generate txt untuk menulis mean, median, modus");
        System.out.println("2. Generate txt untuk grouping data");
        System.out.println("3. Generate txt untuk kedua menu");
        System.out.println("0. Keluar\n");
        System.out.println("pilih: ");

        Scanner input = new Scanner(System.in);
        int pilihan = input.nextInt();
        switch (pilihan) {
            case 0:
                System.exit(0);
            case 1:
                System.out.println("generate file txt untuk menampilkan rata-rata, median, modus: ");
                WriteClass.writeMeanMedianModus();
                result();
                break;
            case 2:
                System.out.print("masukkan nilai: ");
                int input_group = input.nextInt();
                WriteClass.writeGroup(input_group);
                result();
                break;
            case 3:
                System.out.println("masukkan nilai: ");
                int input_group1 = input.nextInt();
                WriteClass.writeMeanMedianModus();
                WriteClass.writeGroup(input_group1);
                result();
                break;
            default:
                System.out.println("pilihan tidak ada");
                mainMenu();
                break;
        }
    }

    public static void result() {

        System.out.println(top);
        System.out.println(title);
        System.out.println(top);
        System.out.println("\nFile telah digenerate ");
        System.out.println("silakan cek\n");
        System.out.println("Pilih menu");
        System.out.println("0. Exit");
        System.out.println("1. Kembali ke menu utama");
        System.out.println("pilih: ");

        Scanner input = new Scanner(System.in);
        int pilihan = input.nextInt();
        switch (pilihan) {
            case 0:
                System.exit(0);
            case 1:
                mainMenu();
                break;
        }
    }



    public static void resultFailed() {

        System.out.println(top);
        System.out.println(title);
        System.out.println(top);
        System.out.println("File tidak ditemukan ");
        System.out.println("\n");
        System.out.println("masukan input untuk keluar");

        Scanner input = new Scanner(System.in);
        int pilihan = input.nextInt();
        if (pilihan >= 'a' && pilihan <= 'z'){
            System.exit(0);
        } else {
            System.exit(0);
        }
    }
}
